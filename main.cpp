#include <GL/glut.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <math.h>
#include <fstream>
#include <string>

using namespace std;

typedef struct{
    float x,y,z;
} point3D_t;

typedef struct{
    float x,y,z,r,g,b,s;
} point3D_color_t;

typedef struct{
    int NumberofVertices;
    short int pnt[32];
} face_t;

typedef struct
{
    float m[3][3];
} matrix3D_t;

typedef struct{
    int NumberofVertices;
    point3D_t pnt[100];
    int NumberofFaces;
    face_t fc[120];
} object3D_t;

typedef struct{
    int NumberofVertices;
    point3D_color_t pnt[100];
    int NumberofFaces;
    face_t fc[120];
} object3D_color_t;

typedef struct{
    float v[3];
} vector3D_t;

typedef struct {
    float r;
    float g;
    float b;
} color_t;

matrix3D_t createIdentity(){
    matrix3D_t rotate;
    rotate.m[0][0]=0.0;
    rotate.m[0][1]=0.0;
    rotate.m[0][2]=0.0;

    rotate.m[1][0]=0.0;
    rotate.m[1][1]=0.0;
    rotate.m[1][2]=0.0;

    rotate.m[2][0]=0.0;
    rotate.m[2][1]=0.0;
    rotate.m[2][1]=0.0;
    return rotate;
}

matrix3D_t rotationX(float teta){
    matrix3D_t rotate=createIdentity();
    rotate.m[0][0]=1.0;
    rotate.m[0][1]=0.0;
    rotate.m[0][2]=0.0;

    rotate.m[1][0]=0.0;
    rotate.m[1][1]=cos(teta/57.3);
    rotate.m[1][2]=-sin(teta/57.3);

    rotate.m[2][0]=0.0;
    rotate.m[2][1]=sin(teta/57.3);
    rotate.m[2][2]=cos(teta/57.3);

    return rotate;
}

matrix3D_t rotationY(float teta){
    matrix3D_t rotate=createIdentity();
    rotate.m[0][0]=cos(teta/57.3);
    rotate.m[0][1]=0.0;
    rotate.m[0][2]=sin(teta/57.3);

    rotate.m[1][0]=0.0;
    rotate.m[1][1]=1.0;
    rotate.m[1][2]=0.0;

    rotate.m[2][0]=-sin(teta/57.3);
    rotate.m[2][1]=0.0;
    rotate.m[2][2]=cos(teta/57.3);

    return rotate;
}

matrix3D_t rotationZ(float teta){
    matrix3D_t rotate=createIdentity();
    rotate.m[0][0]=cos(teta/57.3);
    rotate.m[0][1]=-sin(teta/57.3);
    rotate.m[0][2]=0.0;

    rotate.m[1][0]=sin(teta/57.3);
    rotate.m[1][1]=cos(teta/57.3);
    rotate.m[1][2]=0.0;

    rotate.m[2][0]=0.0;
    rotate.m[2][1]=0.0;
    rotate.m[2][2]=1.0;

    return rotate;
}

matrix3D_t scale3D(float scale){
	scale = scale/100;
    matrix3D_t scaling;
    scaling.m[0][0]=scale;  scaling.m[0][1]=0;   scaling.m[0][2]=0.0;
    scaling.m[1][0]=0.0; scaling.m[1][1]=scale;  scaling.m[1][2]=0.0;
    scaling.m[2][0]=0.0; scaling.m[2][1]=0.0; scaling.m[2][2]=scale;
    return scaling;
}

vector3D_t operator *(matrix3D_t a, vector3D_t b){
    vector3D_t c;
    for(int i=0; i<3; i++){
        c.v[i]=0;
        for(int j=0; j<3; j++){
            c.v[i]+=a.m[i][j]*b.v[j];
        }
    }
    return c;
}

void create3DObject(object3D_t object, color_t col){

    for(int i=0; i<object.NumberofFaces; i++){
        //setColor(col);
        glBegin(GL_LINE_LOOP);
        for(int j=0; j<object.fc[i].NumberofVertices; j++){
            int p=object.fc[i].pnt[j];
            float x=object.pnt[p].x;
            float y=object.pnt[p].y;
            float z=object.pnt[p].z;
            glVertex3f(x, y, 0.0);
        }
    glEnd();
    }

}

void create3DObject_color(object3D_color_t object){

    for(int i=0; i<object.NumberofFaces; i++){
        glBegin(GL_LINE_LOOP);
        for(int j=0; j<object.fc[i].NumberofVertices; j++){
            int p=object.fc[i].pnt[j];
            float x=object.pnt[p].x;
            float y=object.pnt[p].y;
            float z=object.pnt[p].z;
            float r=object.pnt[p].r/object.pnt[p].s;
            float g=object.pnt[p].g/object.pnt[p].s;
            float b=object.pnt[p].b/object.pnt[p].s;
            //setColor((color_t){r,g,b});
            //glColor3f(r,g,b);
            glVertex3f(x, y, 0.0);
        }
    glEnd();
    }

}

void writeOFF(object3D_t obj){
	string filename = "D:/sembilan.off";
	ofstream file;
        file.open(filename.c_str());
        file << "COFF\n";

        file << obj.NumberofVertices << " " << obj.NumberofFaces << " " << "0" << "\n";

        for(int i=0; i<obj.NumberofVertices; i++)
        {
            file << obj.pnt[i].x << " " << obj.pnt[i].y << " " << obj.pnt[i].z << "\n";
        }

        for(int j=0; j<obj.NumberofFaces; j++)
        {
            file << obj.fc[j].NumberofVertices << " ";
            for(int k=0; k<obj.fc[j].NumberofVertices; k++)
            {
                file << obj.fc[j].pnt[k] << " ";
            }
            file << " " << "\n";
        }
    cout << filename << " succesfully wrote.";
}

void writeOFF_color(object3D_color_t obj){
	string filename = "D:/94.off";
	ofstream file;
        file.open(filename.c_str());
        file << "COFF\n";

        file << obj.NumberofVertices << " " << obj.NumberofFaces << " " << "0" << "\n";

        for(int i=0; i<obj.NumberofVertices; i++)
        {
            file << obj.pnt[i].x << " " << obj.pnt[i].y << " " << obj.pnt[i].z << " " << obj.pnt[i].r << " " << obj.pnt[i].g << " " << obj.pnt[i].b << " " << obj.pnt[i].s << "\n";
        }

        for(int j=0; j<obj.NumberofFaces; j++)
        {
            file << obj.fc[j].NumberofVertices << " ";
            for(int k=0; k<obj.fc[j].NumberofVertices; k++)
            {
                file << obj.fc[j].pnt[k] << " ";
            }
            file << " " << "\n";
        }
    cout << filename << " succesfully wrote.";
}

object3D_t readOFF (string filename) {
  object3D_t object;
  ifstream myfile (filename.c_str());
  string coff;
  string temp;

    if(myfile.is_open()){
        getline(myfile,coff); //get coff

        myfile >> object.NumberofVertices;
        cout << object.NumberofVertices;
        myfile >> object.NumberofFaces;
        cout << object.NumberofFaces;
        myfile >> temp;
        cout << "\n";

        for(int i = 0 ; i < object.NumberofVertices; i ++){
            myfile >> object.pnt[i].x;
            myfile >> object.pnt[i].y;
            myfile >> object.pnt[i].z;
            cout << "x : " << object.pnt[i].x << " ";
            cout << "y : " << object.pnt[i].y << " ";
            cout << "z : " << object.pnt[i].z << "\n";
        }

        for(int i = 0 ; i < object.NumberofFaces; i++){
            myfile >> object.fc[i].NumberofVertices;
            cout << object.fc[i].NumberofVertices;

            for(int j = 0; j < object.fc[i].NumberofVertices; j ++){
                myfile >> object.fc[i].pnt[j];
                cout << object.fc[i].pnt[j] << " ";
            }
            cout << "\n";
        }
        return object;
    }


  else cout << "Unable to open file";

  return object;
}

object3D_color_t readOFF_color (string filename) {
  object3D_color_t object;
  ifstream myfile (filename.c_str());
  string coff;
  string temp;

    if(myfile.is_open()){
        getline(myfile,coff); //get coff

        myfile >> object.NumberofVertices;
        cout << object.NumberofVertices;
        myfile >> object.NumberofFaces;
        cout << object.NumberofFaces;
        myfile >> temp;
        cout << "\n";

        for(int i = 0 ; i < object.NumberofVertices; i ++){
            myfile >> object.pnt[i].x;
            myfile >> object.pnt[i].y;
            myfile >> object.pnt[i].z;
            myfile >> object.pnt[i].r;
            myfile >> object.pnt[i].g;
            myfile >> object.pnt[i].b;
            myfile >> object.pnt[i].s;
            cout << "x : " << object.pnt[i].x << " ";
            cout << "y : " << object.pnt[i].y << " ";
            cout << "z : " << object.pnt[i].z << " ";
            cout << "r : " << object.pnt[i].r << " ";
            cout << "g : " << object.pnt[i].g << " ";
            cout << "b : " << object.pnt[i].b << " ";
            cout << "s : " << object.pnt[i].s << "\n";
        }

        for(int i = 0 ; i < object.NumberofFaces; i++){
            myfile >> object.fc[i].NumberofVertices;
            cout << object.fc[i].NumberofVertices;

            for(int j = 0; j < object.fc[i].NumberofVertices; j ++){
                myfile >> object.fc[i].pnt[j];
                cout << object.fc[i].pnt[j] << " ";
            }
            cout << "\n";
        }
        return object;
    }


  else cout << "Unable to open file";

  return object;
}

float sudut = 0;
    /* Test Case 1. Object dideklarasikan di luar userdraw */
    /*
object3D_t sembilan={38,
					 {{-150,-150,50},{-50,-150,50},{50,-150,50},{50,-50,50},{-50,-50,50},{-150,50,50},{-150,150,50},{-50,250,50},{50,250,50},{150,150,50}
					 ,{150,50,50},{150,-50,50},{150,-150,50},{50,-250,50},{-50,-250,50},{-50,50,50},{50,50,50},{50,150,50},{-50,150,50},{-150,-150,-50}
					 ,{-50,-150,-50},{50,-150,-50},{50,-50,-50},{-50,-50,-50},{-150,50,-50},{-150,150,-50},{-50,250,-50},{50,250,-50},{150,150,-50},{150,50,-50}
					 ,{150,-50,-50},{150,-150,-50},{50,-250,-50},{-50,-250,-50},{-50,50,-50},{50,50,-50},{50,150,-50},{-50,150,-50}}, // 38
                       76,
					 {{3, { 0, 1,14}},{3, { 1, 2,14}},{3, { 2,13,14}},{3, { 2,12,13}},{3, { 2,11,12}},{3, { 2, 3,11}},{3, { 3,10,11}},{3, { 3,10,16}},{3, { 3, 4,16}},{3, { 4,15,16}},
					  {3, { 4, 5,15}},{3, { 5,15,18}},{3, { 5, 6,18}},{3, { 6, 7,18}},{3, { 7, 8,18}},{3, { 8,17,18}},{3, { 8, 9,17}},{3, { 9,16,17}},{3, { 9,10,16}}, // Atas
					  {3, {15,16,35}},{3, {16,17,35}},{3, {17,35,36}},{3, {17,36,37}},{3, {17,18,37}},{3, {18,15,37}},{3, {15,34,37}},{3, {15,34,35}}, // Lingkaran dalam
					  {3, { 0, 1,19}},{3, { 1,19,20}},{3, { 1, 2,20}},{3, { 2,20,21}},{3, { 2, 3,21}},{3, { 3,21,22}},{3, { 3, 4,22}},{3, { 4,22,23}},{3, { 4, 5, 23}},{3, { 5,23,24}},
					  {3, { 5, 6,24}},{3, { 6,24,25}},{3, { 6, 7,25}},{3, { 7,25,26}},{3, { 7, 8,26}},{3, { 8,26,27}},{3, { 8, 9,27}},{3, { 9,27,28}},{3, { 9,10,28}},{3, {10,28,29}},
					  {3, {10,11,29}},{3, {11,29,30}},{3, {11,12,30}},{3, {12,30,31}},{3, {12,13,31}},{3, {13,31,32}},{3, {13,14,32}},{3, {14,32,33}},{3, { 0,14,19}},{3, {14,19,33}}, // Tepi luar
					  {3, {19,20,33}},{3, {20,21,33}},{3, {21,32,33}},{3, {21,31,32}},{3, {21,30,31}},{3, {21,22,30}},{3, {22,29,30}},{3, {22,29,35}},{3, {22,23,35}},{3, {23,34,35}},
					  {3, {23,24,34}},{3, {24,34,37}},{3, {24,25,37}},{3, {25,26,37}},{3, {26,27,37}},{3, {27,36,37}},{3, {27,28,36}},{3, {28,35,36}},{3, {28,29,35}}} // Bawah
					 };
					 /**/
	object3D_t sembilan;
	object3D_color_t sembilanCol;
	int coba=true;
	object3D_color_t obj;

void userdraw(void){
	/* Test Case 2. Object dideklarasikan di dalam userdraw */
    
    sembilanCol={38,
					 {{-150,-150,50,255,200,150,255},{-50,-150,50,255,200,150,255},{50,-150,50,50,200,150,255},{50,-50,50,255,50,150,255},{-50,-50,50,255,50,150,255},{-150,50,50,255,50,150,255},{-150,150,50,255,200,150,255},{-50,250,50,255,200,150,255},{50,250,50,255,50,150,255},{150,150,50,255,200,150,255}
					 ,{150,50,50,255,150,150,255},{150,-50,50,255,200,150,255},{150,-150,50,150,200,150,255},{50,-250,50,255,200,150,255},{-50,-250,50,255,200,150,255},{-50,50,50,255,200,150,255},{50,50,50,255,200,150,255},{50,150,50,255,200,150,255},{-50,150,50,255,200,150,255},{-150,-150,-50,255,200,150,255}
					 ,{-50,-150,-50,255,200,150,255},{50,-150,-50,150,200,150,255},{50,-50,-50,255,200,150,255},{-50,-50,-50,255,200,50,255},{-150,50,-50,255,200,50,255},{-150,150,-50,255,200,150,255},{-50,250,-50,255,200,150,255},{50,250,-50,255,50,150,255},{150,150,-50,255,200,150,255},{150,50,-50,255,50,150,255}
					 ,{150,-50,-50,255,150,150,255},{150,-150,-50,255,200,150,255},{50,-250,-50,50,200,150,255},{-50,-250,-50,150,200,15,255},{-50,50,-50,255,200,150,255},{50,50,-50,50,200,150,255},{50,150,-50,255,200,50,255},{-50,150,-50,255,200,150,255}}, // 38
                       76,
					 {{3, { 0, 1,14}},{3, { 1, 2,14}},{3, { 2,13,14}},{3, { 2,12,13}},{3, { 2,11,12}},{3, { 2, 3,11}},{3, { 3,10,11}},{3, { 3,10,16}},{3, { 3, 4,16}},{3, { 4,15,16}},
					  {3, { 4, 5,15}},{3, { 5,15,18}},{3, { 5, 6,18}},{3, { 6, 7,18}},{3, { 7, 8,18}},{3, { 8,17,18}},{3, { 8, 9,17}},{3, { 9,16,17}},{3, { 9,10,16}}, // Atas
					  {3, {15,16,35}},{3, {16,17,35}},{3, {17,35,36}},{3, {17,36,37}},{3, {17,18,37}},{3, {18,15,37}},{3, {15,34,37}},{3, {15,34,35}}, // Lingkaran dalam
					  {3, { 0, 1,19}},{3, { 1,19,20}},{3, { 1, 2,20}},{3, { 2,20,21}},{3, { 2, 3,21}},{3, { 3,21,22}},{3, { 3, 4,22}},{3, { 4,22,23}},{3, { 4, 5, 23}},{3, { 5,23,24}},
					  {3, { 5, 6,24}},{3, { 6,24,25}},{3, { 6, 7,25}},{3, { 7,25,26}},{3, { 7, 8,26}},{3, { 8,26,27}},{3, { 8, 9,27}},{3, { 9,27,28}},{3, { 9,10,28}},{3, {10,28,29}},
					  {3, {10,11,29}},{3, {11,29,30}},{3, {11,12,30}},{3, {12,30,31}},{3, {12,13,31}},{3, {13,31,32}},{3, {13,14,32}},{3, {14,32,33}},{3, { 0,14,19}},{3, {14,19,33}}, // Tepi luar
					  {3, {19,20,33}},{3, {20,21,33}},{3, {21,32,33}},{3, {21,31,32}},{3, {21,30,31}},{3, {21,22,30}},{3, {22,29,30}},{3, {22,29,35}},{3, {22,23,35}},{3, {23,34,35}},
					  {3, {23,24,34}},{3, {24,34,37}},{3, {24,25,37}},{3, {25,26,37}},{3, {26,27,37}},{3, {27,36,37}},{3, {27,28,36}},{3, {28,35,36}},{3, {28,29,35}}} // Bawah
					 };
	/**/
    
    matrix3D_t matrix_X=rotationX(90);
    matrix3D_t matrix_Y=rotationY(sudut);
    matrix3D_t matrix_Z=rotationZ(sudut);
    matrix3D_t matrix_scale=scale3D(25); // range 0-100
    for(int i=0; i<sembilanCol.NumberofVertices; i++){         //teta=15.0;
        vector3D_t p;
        p.v[0]=sembilanCol.pnt[i].x;
        p.v[1]=sembilanCol.pnt[i].y;
        p.v[2]=sembilanCol.pnt[i].z;
        p=(matrix_Y)*(p);
        p=(matrix_X)*(p);
        p=(matrix_Z)*(p);
        p=(matrix_scale)*(p);
        sembilanCol.pnt[i].x=p.v[0];
        sembilanCol.pnt[i].y=p.v[1];
        sembilanCol.pnt[i].z=p.v[2];
    }
	while(coba){
		writeOFF_color(sembilanCol);
    	obj = readOFF_color("D:/Kuliah/Semester 5/Grafika Komputer/Dataset3D/F.off");
		coba = false;
	}
    color_t color1;
    color1.r=1.0;
    color1.g=0.0;
    color1.b=1.0;
    sudut++;
    if(sudut>=360.0)
        sudut=0.0;
    create3DObject_color(sembilanCol);
    //create3DObject_color(obj);
    glFlush();
}
void display(void){
    glClear(GL_COLOR_BUFFER_BIT);
    userdraw();
    glutSwapBuffers();
}

void timer (int value){
    glutPostRedisplay();
    glutTimerFunc(50,timer,0);
}

int main(int argc, char **argv){
    glutInit(&argc,argv);
    glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB );
    glutInitWindowPosition(100,100);
    glutInitWindowSize(640,480);
    glutCreateWindow ("Setiawan Joko | 2103171011");
    glClearColor(0.0, 0.0, 0.0, 0.0);
    gluOrtho2D(-320., 320., -240.0, 240.0);
    //glutIdleFunc(display);
    glutTimerFunc(1,timer,0);
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
